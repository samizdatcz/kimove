title: "Bludištěm rodu Kimů."
perex: "Odhalte nejtajnější dynastii světa v interaktivním rodokmenu"
authors: ["Magdalena Slezáková", "Jan Cibulka"]
coverimg: https://interaktivni.rozhlas.cz/data/kimove/media/cover.jpg
coverimg_note: "Slunce Koreje Kim Ir-sen (uprostřed) obklopen členy své mocné rodiny. Zleva manželka Kim Čong-suk, syn Kim Čong-il, snacha Song Hje-rim, syn Šura, pravnuk Kim Han-sol, vnuk Kim Čong-un, snacha Ko Jong-hui, vnuk Kim Čong-čchol. Koláž iROZHLAS"
styles: []
libraries: ["https://d3js.org/d3.v3.min.js", "https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"]
---

**Patříte k těm, kdo si po únorové vraždě Kim Čong-nama lámali hlavu, o kterého Kima vlastně jde? Teď už nemusíte. iROZHLAS přináší podrobný interaktivní rodokmen nejuzavřenější dynastie světa, která je srdcem totalitního režimu KLDR. Od Slunce národa k jeho pravnukům, od komunismu k nacionalismu: projděte se dějinami rodu Kimů.**

_„Dědičné nástupnictví je reakčním obyčejem vykořisťovatelských společností, kde některé pozice nebo bohatství lze legálně zdědit. Zrodil se v otrokářských společnostech a později ho přejala feudální moc jako prostředek zachování diktátorské vlády.“_

Tolik vydání severokorejského Slovníku politické terminologie z roku 1970. Z dalšího vydání o pár let později zmizela, jako by nikdy neexistovala. Není divu: v tu dobu už pod povrchem KLDR zuřila válka o moc mezi potomky nejvyššího vůdce Kim Ir-sena.

Nástupnictví Kim Čong-ila potvrdilo vznik dynastie, která ve světě nemá obdoby. Rod Kimů tu citovanou definici dovedl k dokonalosti: přivlastnil si nejen moc a bohatství, ale také identitu boha. A zavinil tak smrt milionů lidí.

iROZHLAS.cz zmapoval dynastické vazby i vzájemné boje a sestavil z nich interaktivní rodokmen. Za oponu, která mocnou rodinu tají očím světa, není lehké proniknout; mnohé zpochybňujeme, netušíme nebo se nikdy ani nedozvíme. Tohle je přehled toho mála, co svět ví –  nebo si to aspoň myslí.

_Jednotliví členové dynastie jsou propojeni příbuzenskými liniemi. U každého je připsán vztah, který ho pojí s člověkem nad ním. Kliknutí na vybraný medailonek vám odhalí portrét a příběh, který se za tím kterým příslušníkem rodu skrývá._

<div id="wrapper_mob">
	<div id="chart"></div>
</div>