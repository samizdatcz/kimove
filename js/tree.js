var margin = {top: 40, right: 120, bottom: 20, left: 120},
	width = 1250 - margin.right - margin.left,
	height = 1200 - margin.top - margin.bottom;
	
var i = 0;

var tree = d3.layout.tree()
	.size([height, width]);

var diagonal = d3.svg.diagonal()
	.projection(function(d) { return [d.x, d.y]; });

var svg = d3.select("#chart").append("svg")
	.attr("width", width + margin.right + margin.left)
	.attr("height", height + margin.top + margin.bottom)
  .append("g")
	.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

root = treeData[0];
  
update(root);

function update(source) {

  // Compute the new tree layout.
  var nodes = tree.nodes(root).reverse(),
	  links = tree.links(nodes);

  // Normalize for fixed-depth.
  nodes.forEach(function(d) { d.y = d.depth * 120; });

  // Declare the nodes…
  var node = svg.selectAll("g.node")
	  .data(nodes, function(d) { return d.id || (d.id = ++i); });

  // Enter the nodes.
  var nodeEnter = node.enter().append("g")
	  .attr("class", "node")
	  .attr("transform", function(d) { 
		  return "translate(" + d.x + "," + d.y + ")"; });

  nodeEnter.append("circle")
	  .attr("r", function (d) {
	  	if (d.important == true) {
	  		return 20
	  	} else {
	  		return 10
	  	}
	  })
	  .style("fill", "#fff")
	  .style("stroke", function(d) {
	  	if (d.sex == "man") {
	  		return "steelblue"
	  	} else if (d.sex == "woman") {
	  		return "red"
	  	} else {
	  		return "gray"
	  	}
	  });

  nodeEnter.append("image")
      .attr("xlink:href", function(d) {
      	if (d.important == true) {
      		return 'https://interaktivni.rozhlas.cz/data/kimove/photos/' + d.name + '_round.png'
      	} else {
      		return null
      	}
      })
      .attr("x", -20)
      .attr("y", -20)
      .attr("width", 39)
      .attr("height", 39);


  nodeEnter.append("text")
  		.attr("x", function(d) {
  			if (d.name == "Kim Pchjong-il" | d.name == "Kim Sol-hui") {
  				return -14;
  			} else {
  				return 0;
  			}
  		})
	  .attr("y", function(d) {
	  	if (d.important == true) {
	  		return d.children || d._children ? -28 : 28;
	  	} else {
	  		return d.children || d._children ? -18 : 18;
	  	}   
	  	})
	  .attr("dy", ".35em")
	  .attr("text-anchor", "middle")
	  .text(function(d) { return d.name; })
	  .style("fill-opacity", 1);

// druhy popisek
nodeEnter.append("text")
  		.attr("x", function(d) {
  			if (d.name == "Kim Pchjong-il" | d.name == "Kim Sol-hui") {
  				return -14;
  			} else {
  				return 0;
  			}
  		})
	  .attr("y", function(d) {
	  	if (d.important == true) {
	  		return d.children || d._children ? -38 : 38;
	  	} else {
	  		return d.children || d._children ? -28 : 28;
	  	}   
	  	})
	  .attr("dy", ".35em")
	  .attr("text-anchor", "middle")
	  .style("font-style", "italic")
	  .text(function(d) { return popisek[d.name]; })
	  .style("fill-opacity", 1);

  // Declare the links…
  var link = svg.selectAll("path.link")
	  .data(links, function(d) { return d.target.id; });

  // Enter the links.
  link.enter().insert("path", "g")
	  .attr("class", "link")
	  .attr("d", diagonal);

	  //clicks
	nodeEnter.on("click", function (e) {
		$.magnificPopup.open({
        items: {
          src: '<div class="white-popup"><h2>' + e.name + ' ' + letopocty[e.name] + '</h2>'
          + '<img src="https://interaktivni.rozhlas.cz/data/kimove/photos/' + e.name + '.jpg" height="130"><br><br>'
          + '<p class="kim_text" style="display: inline-block;">' + texts[e.name] + '</p>'
          + '</div>',
          type: 'inline'
        }
      }, 0);
	})

}