var treeData = [{
    "name": "Kim Hjong-čik",
    "sex": "man",
    "children": [{
        "name": "Kang Pan-suk",
        "sex": "woman",
        "children": [{
            "name": "Kim Jong-ču",
            "sex": "man"
        }, {
            "name": "KIM IR-SEN",
            "sex": "man",
            "important": true,
            "children": [{
                    "name": "Kim Čong-suk",
                    "sex": "woman",
                    "children": [{
                            "name": "KIM ČONG-IL",
                            "sex": "man",
                            "important": true,
                            "children": [{
                                    "name": "Hong Il-čchon",
                                    "sex": "woman",
                                    "children": [{
                                        "name": "Kim Hje-kjong",
                                        "sex": "woman",
                                    }]
                                },
                                {
                                    "name": "Song Hje-rim",
                                    "sex": "woman",
                                    "children": [{
                                            "name": "Kim Čong-nam",
                                            "sex": "man",
                                            "children": [{
                                                "name": "I Hje-kjong",
                                                "sex": "woman",
                                                "children": [
                                                    {
                                                        "name": "Kim Han-sol",
                                                        "sex": "man",
                                                    }
                                                ]
                                            }]
                                        }

                                    ]
                                },
                                {
                                    "name": "Kim Jong-suk",
                                    "sex": "woman",
                                    "children": [{
                                            "name": "Kim Sol-song",
                                            "sex": "woman",
                                        }

                                    ]
                                },
                                {
                                    "name": "Ko Jong-hui",
                                    "sex": "woman",
                                    "children": [{
                                            "name": "Kim Čong-čchol",
                                            "sex": "man",
                                            "children": [{
                                                    "name": "Neznámá",
                                                    "sex": "woman",
                                                    "children": [{
                                                        "name": "Neznámé",
                                                        "sex": "",
                                                    }]
                                                }

                                            ]
                                        },
                                        {
                                            "name": "KIM ČONG-UN",
                                            "sex": "man",
                                            "important": true,
                                            "children": [{
                                                    "name": "Ri Sol-ču",
                                                    "sex": "woman",
                                                    "children": [{
                                                            "name": "Neznámé ",
                                                            "sex": "",
                                                        },
                                                        {
                                                            "name": "Kim Ču-e",
                                                            "sex": "woman",
                                                        }

                                                    ]
                                                }

                                            ]
                                        },
                                        {
                                            "name": "Kim Jo-čong",
                                            "sex": "woman",
                                        }
                                    ]
                                },
                                {
                                    "name": "Kim Ok",
                                    "sex": "woman",
                                }
                            ]
                        },
                        {
                            "name": "Kim Man-il",
                            "sex": "man",
                        },
                        {
                            "name": "Kim Kjong-hui",
                            "sex": "woman",
                            "children": [{
                                "name": "Čang Song-tchek",
                                "sex": "man"
                            }]
                        }
                    ]
                },
                {
                    "name": "Kim Song-e",
                    "sex": "woman",
                    "children": [{
                            "name": "Kim Kjong-čin",
                            "sex": "woman",
                            "children": [{
                                "name": "Kim Kwang-sop",
                                "sex": "man",
                            }]
                        },
                        {
                            "name": "Kim Pchjong-il",
                            "sex": "man",
                        },
                        {
                            "name": "Kim Jong-il",
                            "sex": "man"
                        }  
                    ]
                },
                {
                    "name": "Ošetřovatelka",
                    "sex": "woman",
                    "children": [{
                        "name": "Kim Hjun",
                        "sex": "man",
                    }]
                }
            ]
        }]
    }]
}];